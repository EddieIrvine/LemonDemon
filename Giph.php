<?php

	set_error_handler(
	    create_function(
	        '$severity, $message, $file, $line',
	        'throw new ErrorException($message, $severity, $severity, $file, $line);'
	    )
	);

	class Giph
	{
		private $likes, $dislikes;
		private $url;

		private $dbHost, $dbUser, $dbPassword, $dbName;

		public function __construct($url)
		{
			$this->dbHost = "localhost";
			$this->dbUser = "root";
			$this->dbPassword = "";
			$this->dbName = "lemonD";

			$this->url = $url;

			try
			{
				$dbConnection = @new mysqli($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);

				$url = htmlentities($url, ENT_QUOTES, "UTF-8");

				$sql = sprintf("SELECT * FROM giphs WHERE url='%s'", mysqli_escape_string($dbConnection, $url));

				$result = $dbConnection->query($sql);

				if ($result->num_rows)
				{
					$verse = $result->fetch_assoc();

					$this->likes = $verse['likes'];
					$this->dislikes = $verse['dislikes'];
				}
				else
				{
					$this->likes = 0;
					$this->dislikes = 0;
				}

				$result->free();
				$dbConnection->close();
			}
			catch (Exception $ex)
			{
				echo "Błąd połączenia z bazą!<br /><br />";

				$this->likes = 0;
				$this->dislikes = 0;
			}
		}

		public function __toString()
		{
			$url = $this->url;

			$likes = $this->likes;
			$dislikes = $this->dislikes;

			$string = '<br /><br />';

			$string = $string. '<img src="'.$url.'" />';

			$string = $string. '<form method="post" action="rate.php">';

				$string = $string. 'Like: '. $likes. ' Dislike: '. $dislikes. '<br />';
				
				$string = $string. '<input type="submit" name="like" value="like" />';
				$string = $string. '<input type="submit" name="dislike" value="dislike" />';

				$string = $string. '<input type="hidden" name="giphUrl" value="'.$url .'" />';

			$string = $string. '</form>';

			return $string;
		}

		public function getGiphId()
		{
			try
			{
				$dbConnection = @new mysqli($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);
			}
			catch (Exception $ex)
			{
				echo "Błąd połączenia z bazą danych";

				return -1;
			}
			
			$url = htmlentities($this->url, ENT_QUOTES, "UTF-8");
			$sql = sprintf("SELECT id FROM giphs WHERE url='%s'", mysqli_escape_string($dbConnection, $url));

			$result = $dbConnection->query($sql);

			if ($result->num_rows)
			{
				$verse = $result->fetch_assoc();

				$id = $verse['id'];

				$result->free();
				$dbConnection->close();

				return $id;
			}
			else
			{
				$result->free();
				$dbConnection->close();

				return -1;
			}
		}

		public function getIpAddressId()
		{
			try
			{
				$dbConnection = @new mysqli($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);
			}
			catch (Exception $ex)
			{
				echo "Błąd połączenia z bazą danych";

				return -1;
			}

			$ipAddress = $_SERVER['REMOTE_ADDR'];

			$sql = "SELECT * FROM ipAddresses WHERE ipAddress='$ipAddress'";

			$result = $dbConnection->query($sql);

			if ($result->num_rows)
			{
				$verse = $result->fetch_assoc();

				$id = $verse['id'];

				$result->free();
				$dbConnection->close();

				return $id;
			}
			else 
			{
				$result->free();
				$dbConnection->close();

				return -1;
			}
		}

		public function pushGiph()
		{
			try
			{
				$dbConnection = @new mysqli($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);
			}
			catch (Exception $ex)
			{
				echo "Błąd połączenia z bazą danych";

				return false;
			}

			$url = htmlentities($this->url, ENT_QUOTES, "UTF-8");

			$sql = sprintf("SELECT * FROM giphs WHERE url='%s'", mysqli_escape_string($dbConnection, $url));

			$result = $dbConnection->query($sql);

			if (!$result->num_rows)
			{
				$dbConnection->query("INSERT INTO giphs VALUES (NULL, '$url', $this->likes, $this->dislikes)");
				$dbConnection->close();

				$result->free();

				return true;
			}
			else 
			{
				$result->free();

				$dbConnection->close();

				return false;
			}
		}

		public function pushIpAddress()
		{
			try
			{
				$dbConnection = @new mysqli($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);
			}
			catch (Exception $ex)
			{
				echo "Błąd połączenia z bazą danych";

				return false;
			}

			$ipAddress = $_SERVER['REMOTE_ADDR'];

			$sql = "SELECT * FROM ipAddresses WHERE ipAddress='$ipAddress'";

			$result = $dbConnection->query($sql);

			if (!$result->num_rows)
			{
				$dbConnection->query("INSERT INTO ipAddresses VALUES (NULL, '$ipAddress')");
				$dbConnection->close();

				return true;
			}
			else 
			{
				$result->free();

				$dbConnection->close();

				return false;
			}

		}

		public function rateGiph($rate)
		{
			try
			{
				$dbConnection = @new mysqli($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);
			}
			catch (Exception $ex)
			{
				echo "Błąd połączenia z bazą danych";

				return false;
			}

			$this->pushGiph();
			$this->pushIpAddress();

			$ipAddress = $_SERVER['REMOTE_ADDR'];

			$url = htmlentities($this->url, ENT_QUOTES, "UTF-8");
			$sql = sprintf("SELECT rates.id, rates.rate FROM rates INNER JOIN giphs ON rates.giphId = giphs.id INNER JOIN ipAddresses ON rates.ipAddressId = ipAddresses.id WHERE giphs.url = '%s' AND ipAddresses.ipAddress = '$ipAddress'", mysqli_escape_string($dbConnection, $url));

			$result = $dbConnection->query($sql);

			if (!$result->num_rows)
			{
				$giphId = $this->getGiphId();
				$ipAddressId = $this->getIpAddressId();

				$rate = htmlentities($rate, ENT_QUOTES, "UTF-8");
				$rate = mysqli_escape_string($dbConnection, $rate);

				$dbConnection->query("INSERT INTO rates VALUES (NULL, $giphId, $ipAddressId, '$rate')");

				if ($rate === "like")
					$this->likes++;
				else 
					$this->dislikes++;

				$dbConnection->query("UPDATE giphs SET likes = $this->likes WHERE url='$this->url'");
				$dbConnection->query("UPDATE giphs SET dislikes = $this->dislikes WHERE url='$this->url'");

				$result->free();
				$dbConnection->close();

				return true;
			}
			else 
			{
				$verse = $result->fetch_assoc();

				$id = $verse['id'];

				$giphRate = $verse['rate'];

				if ($rate === $giphRate)
				{
					$dbConnection->query("DELETE FROM rates WHERE id = $id");

					if ($rate === "like")
						$this->likes--;
					else 
						$this->dislikes--;
				}
				else 
				{
					$dbConnection->query("UPDATE rates SET rate = '$rate' WHERE id = $id");

					if ($rate === "like")
					{
						$this->likes++;
						$this->dislikes--;
					}
					else
					{ 
						$this->likes--;
						$this->dislikes++;
					}
				}
				
				$dbConnection->query("UPDATE giphs SET likes = $this->likes WHERE url='$this->url'");
				$dbConnection->query("UPDATE giphs SET dislikes = $this->dislikes WHERE url='$this->url'");

				$result->free();
				$dbConnection->close();

				return true;
			}

		}
	}

?>