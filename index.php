<?php

	require_once "Giph.php";

	session_start();

	set_error_handler(
	    create_function(
	        '$severity, $message, $file, $line',
	        'throw new ErrorException($message, $severity, $severity, $file, $line);'
	    )
	);

	if (isset($_GET['search']))
	{
		$query = str_replace(" ", "+", $_GET['search']);

		$_SESSION['search'] = $query;

		$apiKey = "dc6zaTOxFJmzC";
		$giphyUrl = "http://api.giphy.com/v1/gifs/search?q=". $query. "&api_key=". $apiKey;

		try
		{
			$giphyJSON = file_get_contents($giphyUrl);
			$giphyArray = json_decode($giphyJSON, true);
		}
		catch (Exception $exc)
		{
			echo '<span style="color: red;">Błąd połączenia z REST API!</span><br /><br />';	
		}
	}

?>

<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>

<body>

	<form method="get" action="index.php">

		Wpisz szukaną frazę: 
		<input type="text" name="search" />
		<input type="submit" value="Szukaj" />

	</form>

	<?php

		if (!isset($giphyArray))
			exit();

		foreach ($giphyArray['data'] as $giph) 
		{
			$url = $giph['images']['fixed_height']['url'];

			$giph = @new Giph($url);

			echo $giph;
		}

		if (!count($giphyArray['data']))
			echo "<br />Brak rezultatów!";
	?>

</body>
</html>