<?php

	require_once "Giph.php";

	session_start();

	if (!isset($_POST['like']) && !isset($_POST['dislike']))
	{
		header("Location: index.php");
		exit();
	}

	if (isset($_POST['like']))
		$rate = $_POST['like'];
	else 
		$rate = $_POST['dislike'];
	
	$giph = new Giph($_POST['giphUrl']);

	$giph->rateGiph($rate);

	$search = $_SESSION['search'];

	unset($_SESSION['search']);

	header("Location: index.php?search=$search");
?>